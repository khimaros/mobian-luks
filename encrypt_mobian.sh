ORIGLOOP=/dev/loop0
CRYPTLOOP=/dev/loop1
CRYPTROOT=mobian_cryptroot

# Teardown
teardown() {
  if [[ ! -z "${1}" ]]; then echo "${1} Tearing down..."; fi
  sudo mv /mnt/target/etc/resolv.conf.bak /mnt/target/etc/resolv.conf
  sudo rm /mnt/target/bin/qemu-aarch64-static
  sudo umount -R /mnt/target
  sudo umount /mnt/orig
  sudo cryptsetup close /dev/mapper/$CRYPTROOT
  sudo losetup -d $CRYPTLOOP
  sudo losetup -d $ORIGLOOP
  if [[ ! -z "${1}" ]]; then echo "Teardown complete, please fix the issue and then run this script again"; exit 2; fi
}

# Pre-flight checks
[[ $(which qemu-aarch64-static) ]] || { echo "qemu-aarch64-static from package qemu-user-static missing, aborting!"; exit 2; }
if [[ "$(ls mobian-pinephone-*.img | wc -l)" -gt 1 ]]; then 
    echo  "More than one Mobian img in current dir, aborting!"
    exit 2
elif [[ ! "$(ls mobian-pinephone-*.img)" ]]; then
    echo "Mobian image not found, aborting!"
    exit 2
fi

# Sort images
cp mobian-pinephone-*.img mobian-crypt.img
sudo losetup --partscan $ORIGLOOP mobian-pinephone-*.img || teardown "Failed to mount original Mobian image!"
sudo losetup --partscan $CRYPTLOOP mobian-crypt.img || teardown "Failed to mount target encrypted image!"

# Encrypt root partition
sudo cryptsetup luksFormat "$CRYPTLOOP"p2 || teardown "Failed to LUKS format target encrypted image!"
sudo cryptsetup luksOpen "$CRYPTLOOP"p2 $CRYPTROOT || teardown "Failed to unlock target encrypted image!"
sudo mkfs.ext4 /dev/mapper/$CRYPTROOT || teardown "Failed to format target encrypted image as ext4!"

ENCRYPTED_PART_UUID=$(lsblk -o UUID,NAME ${CRYPTLOOP}p2 | grep loop | awk '{ print $1 }')

# Setup directories
sudo mkdir /mnt/orig
sudo mkdir /mnt/target

# Mount and copy root partition content
sudo mount "$ORIGLOOP"p2 /mnt/orig
sudo mount /dev/mapper/$CRYPTROOT /mnt/target || teardown "Failed to mount target encrypted image!" 
sudo cp -ra /mnt/orig/* /mnt/target || teardown "Failed to copy original rootfs to target encrypted image!" 

sudo cp /mnt/target/etc/resolv.conf /mnt/target/etc/resolv.conf.bak 
sudo cp /etc/resolv.conf /mnt/target/etc/resolv.conf || teardown "Failed to copy system resolv.conf to target image!" 
sudo cp /usr/bin/qemu-aarch64-static /mnt/target/bin/ || teardown "Failed to copy qemu-aarch64-static to target image!" 
cat << EOF > install.sh
apt install osk-sdl cryptsetup libsdl2-2.0-0 libsdl2-ttf-2.0-0 -y || exit 4
# Reconfigure boot script
echo "root UUID=${ENCRYPTED_PART_UUID} none luks,keyscrypt=/usr/share/initramfs-tools/scripts/osk-sdl-keyscript" >> /etc/crypttab
sed -i "s:UUID=.*\(\t/\t.*\):/dev/mapper/root\1:" /etc/fstab ||  exit 4
u-boot-update

# Uncomment for old images which do not use u-boot-menu
#sed -i "s:^setenv bootargs.*$:setenv bootargs console=ttyS0,115200 no_console_suspend panic=10 consoleblank=0 loglevel=7 osk-sdl-root=/dev/mmcblk\${linux_mmcdev}p\${rootpart} osk-sdl-root-name=root root=/dev/mapper/root rw splash plymouth.ignore-serial-consoles vt.global_cursor_default=0:" /boot/boot.cmd
#mkimage -T script -C none -A arm64 -d /boot/boot.cmd /boot/boot.scr
EOF
sudo mv install.sh /mnt/target/install.sh || teardown "Failed to move install.sh to target image!"

# setup chroot
sudo mount "$CRYPTLOOP"p1 /mnt/target/boot || teardown "Failed to mount target image /boot partition!"
sudo mount --bind /dev /mnt/target/dev || teardown "Failed to bind /dev!"
sudo mount --bind /sys /mnt/target/sys || teardown "Failed to bind /sys!"
sudo mount --bind /proc /mnt/target/proc || teardown "Failed to bind /proc!"

# Run install script
sudo chroot /mnt/target qemu-aarch64-static /bin/bash /install.sh || teardown "Failed to run install.sh on target image!"

teardown


echo "mobian-crypt.img is now ready to be flashed"
echo "Initramfs generated off device will be significantly larger"
echo "and slower to boot than on-device. Please run"
echo "'update-initramfs -u' after first boot."
